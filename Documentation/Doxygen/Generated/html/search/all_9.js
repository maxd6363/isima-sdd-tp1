var searchData=
[
  ['mag',['MAG',['../color_8h.html#af54a5a977c0c499323d656315f008ee0',1,'color.h']]],
  ['main',['main',['../main_8c.html#abf9e6b7e6f15df4b525a2e7705ba3089',1,'main.c']]],
  ['main_2ec',['main.c',['../main_8c.html',1,'']]],
  ['matrix',['matrix',['../structmatrix__t.html#a2d639e6bdc7698e25016b6dcbf871480',1,'matrix_t']]],
  ['matrix_2ec',['matrix.c',['../matrix_8c.html',1,'']]],
  ['matrix_2eh',['matrix.h',['../matrix_8h.html',1,'']]],
  ['matrix_5ft',['matrix_t',['../structmatrix__t.html',1,'']]],
  ['matrixcreate',['matrixCreate',['../matrix_8c.html#ab0dc0b82bb0286070e913eda7ce1b293',1,'matrixCreate(const char *fileName, matrix_t *matrix):&#160;matrix.c'],['../matrix_8h.html#ab0dc0b82bb0286070e913eda7ce1b293',1,'matrixCreate(const char *fileName, matrix_t *matrix):&#160;matrix.c']]],
  ['matrixfilegenerator',['matrixFileGenerator',['../matrix_8c.html#ac48c38f37b8ca076b1f3de2504f7e8ec',1,'matrixFileGenerator(const char *filename, long rows, long colomns):&#160;matrix.c'],['../matrix_8h.html#ac48c38f37b8ca076b1f3de2504f7e8ec',1,'matrixFileGenerator(const char *filename, long rows, long colomns):&#160;matrix.c']]],
  ['matrixfree',['matrixFree',['../matrix_8c.html#a698c89db63066f504591a2b6fc6d6280',1,'matrixFree(matrix_t *m):&#160;matrix.c'],['../matrix_8h.html#a698c89db63066f504591a2b6fc6d6280',1,'matrixFree(matrix_t *m):&#160;matrix.c']]],
  ['matrixinit',['matrixInit',['../matrix_8c.html#af37694fe5323c42201138f96352bf3f1',1,'matrixInit(matrix_t *out):&#160;matrix.c'],['../matrix_8h.html#af37694fe5323c42201138f96352bf3f1',1,'matrixInit(matrix_t *out):&#160;matrix.c']]],
  ['matrixisempty',['matrixIsEmpty',['../matrix_8c.html#aefa5c075673399f5fc68e90b691d9c8c',1,'matrixIsEmpty(matrix_t m):&#160;matrix.c'],['../matrix_8h.html#aefa5c075673399f5fc68e90b691d9c8c',1,'matrixIsEmpty(matrix_t m):&#160;matrix.c']]],
  ['matrixprint',['matrixPrint',['../matrix_8c.html#a86902bc96274783574c5e162a9f02cef',1,'matrixPrint(matrix_t m):&#160;matrix.c'],['../matrix_8h.html#a86902bc96274783574c5e162a9f02cef',1,'matrixPrint(matrix_t m):&#160;matrix.c']]],
  ['matrixzero',['matrixZero',['../matrix_8c.html#a5f2c071219fa0ae35f741f24b5ed1f0f',1,'matrixZero(void):&#160;matrix.c'],['../matrix_8h.html#a5f2c071219fa0ae35f741f24b5ed1f0f',1,'matrixZero(void):&#160;matrix.c']]],
  ['max_5fstring_5fsize',['MAX_STRING_SIZE',['../string_8h.html#a220f38b26fa99d4d91b574f42d991516',1,'string.h']]],
  ['menu_2ec',['menu.c',['../menu_8c.html',1,'']]],
  ['menu_2eh',['menu.h',['../menu_8h.html',1,'']]],
  ['menuchoice',['menuChoice',['../menu_8h.html#aa2c74f5f61ea334e1aeca479c0ece340',1,'menu.h']]],
  ['menucreate',['menuCreate',['../menu_8c.html#a6e643af5300c2fc3778f7baa57ae7a69',1,'menuCreate(int rows, const char text[][MAX_STRING_SIZE], const char *title, int paddingUp, int paddingBottom, int paddingLeft, int paddingRight):&#160;menu.c'],['../menu_8h.html#a6e643af5300c2fc3778f7baa57ae7a69',1,'menuCreate(int rows, const char text[][MAX_STRING_SIZE], const char *title, int paddingUp, int paddingBottom, int paddingLeft, int paddingRight):&#160;menu.c']]],
  ['menucustom',['menuCustom',['../menu_8c.html#a3693105a7368952eedbcffdbbeae9b01',1,'menuCustom(const char items[][MAX_STRING_SIZE], int rows, const char *title):&#160;menu.c'],['../menu_8h.html#a3693105a7368952eedbcffdbbeae9b01',1,'menuCustom(const char items[][MAX_STRING_SIZE], int rows, const char *title):&#160;menu.c']]],
  ['menumanagement',['menuManagement',['../menu_8c.html#acfee0cfd330e4f1a9ff615af1f60b0bc',1,'menuManagement(const char items[][MAX_STRING_SIZE], int rows, const char *title):&#160;menu.c'],['../menu_8h.html#acfee0cfd330e4f1a9ff615af1f60b0bc',1,'menuManagement(const char items[][MAX_STRING_SIZE], int rows, const char *title):&#160;menu.c']]]
];
