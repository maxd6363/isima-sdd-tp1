var indexSectionsWithContent =
{
  0: "abcdefgklmnoprstuwy",
  1: "mnp",
  2: "bceglmps",
  3: "cdgklmnps",
  4: "cdfmnpr",
  5: "ln",
  6: "be",
  7: "aefnopt",
  8: "bcegmruwy"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

