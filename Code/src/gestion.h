/**
 * @brief      Déclaration de la gestion du programme
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef GESTION
#define GESTION

#define UNDEFINED -1


#include "error.h"
#include "bool.h"

/**
 * @brief      Permet d'avoir l'interface graphique de l'application
 *
 * @return     L'erreur
 */
error_t gestionGraphique(void);


/**
 * @brief      Permet d'avoir la gestion en ligne de commande
 *
 * @param[in]  argc  Le nombre d'arguents
 * @param      argv  Le tableau des arguments
 *
 * @return     L'erreur
 */
error_t gestionArgs(const char * filename, int kvalue, int factory, bool saveFile);


/**
 * @brief      Affiche l'aide pour l'utilisation du programme
 *
 * @return     L'erreur
 */
error_t gestionHelp(void);

#endif