/**
 * @brief      Déclaration de la gestion spécifique de la liste
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */


#ifndef GESTION_LISTE_PRODUCTION
#define GESTION_LISTE_PRODUCTION

#include "error.h"

/**
 * @brief      Charge K valeurs de couts les plus faible dans une liste chainée
 *
 * @param[in]  m     La matrice contenant les valeurs
 * @param      list  La liste
 * @param[in]  k     Le nombre K de valeur à garder
 *
 * @return     L'erreur
 */
error_t listKeepKValues(matrix_t m, list_t *list, int k);



/**
 * @brief      Supprime toutes les occurences d'une usine dans la liste chainée
 *
 * @param      list     La liste
 * @param[in]  factory  L'usine à supprimer
 *
 * @return     L'erreur
 */
error_t listDeleteFactory(list_t *list, int factory);






#endif