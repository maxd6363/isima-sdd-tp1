/**
 * @brief      Implémentation du type production
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

 #include "production.h"

#include <stdio.h>



/**
 * @brief      Affiche une production sur un flot
 *
 * @param      flot  Le fichier de sortie
 * @param[in]  prod  La production
 */
void productionPrint(FILE * flot, production_t prod){
	fprintf(flot, "┃   %6.2f    ┃      %d      ┃      %d      ┃\n",prod.cost,prod.factory,prod.period);
}


/**
 * @brief      Affiche une production sur stdout
 *
 * @param      flot  Le fichier de sortie
 * @param[in]  prod  La production
 */
void productionPrintf(production_t prod){
	productionPrint(stdout, prod);
}

