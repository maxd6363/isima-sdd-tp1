/**
 * @brief      Définition du type matrice et des fonctions associées
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef MATRIX_H
#define MATRIX_H

#include "error.h"
#include "bool.h"

/**
 * Structure pour représenter une matrice
 */
typedef struct {
	int **matrix;
	long rows;
	long cols;
} matrix_t;


/**
 * @brief      Créer un fichier contenant une matrice aléatoire dans le format de l'application
 *
 * @param[in]  filename  Le nom du fichier
 * @param[in]  rows      Le nombre de lignes de la matrice
 * @param[in]  colomns   Le nombre de colonnes de la matrice
 *
 * @return     The error.
 */
error_t matrixFileGenerator(const char * filename, long rows, long colomns);


/**
 * @brief      Initialise une matrice à partir de son nombre de lignes et colonnes
 *
 * @param      out   La matrice initialisé (passage par adresse)
 *
 * @return     L'erreur
 */
error_t matrixInit(matrix_t *out);


/**
 * @brief      Afficher une matrice
 *
 * @param[in]  m     Matrice à afficher
 *                   
 * @return     L'erreur
 */
error_t matrixPrint(matrix_t m);


/**
 * @brief      Met la matrice à une taille 0
 *
 * @return     La matrice
 */
matrix_t matrixZero(void);


/**
 * @brief      Libère la place mémoire alloué pour une matrice
 *
 * @param[in]  m     La matrice à libérer
 *
 * @return     L"erreur
 */
error_t matrixFree(matrix_t *m);


/**
 * @brief      Crée un matrice à partir d'un fichier
 *
 * @param      fileName  Le nom du fichier
 * @param      matrix    La matrice crée (passage par adresse)
 *
 * @return     L'erreur
 */
error_t matrixCreate(const char* fileName, matrix_t* matrix);


/**
 * @brief      Retourne si la metrice
 *
 * @param[in]  m     La matrice à tester
 *
 * @return     Si la matrice est vide
 */
bool matrixIsEmpty(matrix_t m);

#endif