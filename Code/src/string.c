/**
 * @brief      Implémentation des fonctions de traitement de chaines de caractères
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */
#include "string.h"

#include <stdlib.h>

/**
 * @brief      Met en majuscule une chaine de caractère
 *
 * @param      chaine  La chaine à mettre en majuscule
 */
void stringUpper(char* chaine){
	int i=0;
	if(chaine != NULL){		
		while(chaine[i] != '\0'){
			if(chaine[i] >= 'a' && chaine[i] <= 'z'){
				chaine[i]=(int)chaine[i]-32;
			}
			i++;
		}
	}

}


/**
 * @brief      Met en majuscule un caractère
 *
 * @param      chaine  Le caractère
 */
void charUpper(char* c){
	int i=0;
	if(c != NULL){		
		if(*c >= 'a' && *c <= 'z'){
			*c=(int)(*c)-32;
		}
		i++;
		
	}

}
