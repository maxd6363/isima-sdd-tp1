/**
 * @brief      Implémentation du type erreur et des fonctions associées
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include <stdio.h>
#include "error.h"
#include "color.h"


/**
 * @brief      Fonction pour logger les erreurs
 *
 * @param[in]  err   L'erreur
 */
void logError(error_t err) {
	#ifdef VERBOSE
	
	switch(err) {
		case OK:
		fprintf(stderr, GRE "OK");
		break;
		case ERROR:
		fprintf(stderr, RED "ERROR : Erreur non spécifié");
		break;
		case FILE_NOT_FOUND:
		fprintf(stderr, RED "ERROR : Fichier non trouvé");
		break;
		case ALLOC_ERROR:
		fprintf(stderr, RED "ERROR : Erreur allocation");
		break;
		case OUT_OF_BOUNDS:
		fprintf(stderr, RED "ERROR : Index hors limite");
		break;
		case NULL_POINTER:
		fprintf(stderr, RED "ERROR : Pointer null");
		break;
		case NOT_IMPLEMENTED_YET:
		fprintf(stderr, RED "ERROR : Pas encore implementé");
		break;
		case NB_ROWS_INVALID:
		fprintf(stderr, RED "ERROR : Nombre de lignes invalide");
		break;
		case NB_SPACE_INVALID:
		fprintf(stderr, RED "ERROR : Nombre d'espace invalide");
		break;
		case PADDING_UP_INVALID:
		fprintf(stderr, RED "ERROR : Padding haut invalide");
		break;
		case PADDING_DOWN_INVALID:
		fprintf(stderr, RED "ERROR : Padding bas invalide");
		break;
		case PADDING_LEFT_INVALID:
		fprintf(stderr, RED "ERROR : Padding gauche invalide");
		break;
		case PADDING_RIGHT_INVALID:
		fprintf(stderr, RED "ERROR : Padding droit invalide");
		break;
		default:
		fprintf(stderr, RED "ERROR");
		break;
	}
	printf("\n" RESET);

	#endif
}
