/**
 * @brief      Implémentation du type matrice et des fonctions associées
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include "matrix.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "error.h"





/**
 * @brief      Créer un fichier contenant une matrice aléatoire dans le format de l'application
 *
 * @param[in]  filename  Le nom du fichier
 * @param[in]  rows      Le nombre de lignes de la matrice
 * @param[in]  colomns   Le nombre de colonnes de la matrice
 *
 * @return     The error.
 */
error_t matrixFileGenerator(const char * filename, long rows, long colomns){
	error_t error = OK;
	srand(time(NULL));
	
	FILE * flot = fopen(filename,"w");
	if(flot != NULL){
		fprintf(flot, "%02ld %02ld\n",rows, colomns );
		for(int i=0;i<rows;i++){
			for(int j=0;j<colomns;j++){
				fprintf(flot, "%02d ",rand()%100);
			}
			fprintf(flot, "\n");
		}
		fclose(flot);
	}
	else{
		error=FILE_NOT_FOUND;
	}
	return error;
}





/**
 * @brief      Initialise une matrice à partir de son nombre de lignes et colonnes
 *
 * @param      out   La matrice initialisé (passage par adresse)
 *
 * @return     L'erreur
 */
error_t matrixInit(matrix_t *out){
	error_t error = OK;
	int iterator = 0;
	if(out != NULL){
		if(out->rows > 0 && out->cols > 0){
			if (out->matrix != NULL) {
				matrixFree(out);
			}
			out->matrix = (int**)malloc(out->rows * sizeof(int*));
			if(out->matrix==NULL){
				error=ALLOC_ERROR;
			}
			else{
				while(iterator<out->rows && error == 0){
					out->matrix[iterator]=(int*)malloc(out->cols*sizeof(int));
					if(out->matrix[iterator]==NULL){
						error=ALLOC_ERROR;
					}
					iterator++;
				}	
			}
		}
		else{
			error = ERROR;
		}
	}
	else{
		error = NULL_POINTER;
	}
	return error;
}

/**
 * @brief      Met la matrice à une taille 0
 *
 * @return     La matrice
 */
matrix_t matrixZero(void){
	matrix_t matrix = {NULL,0,0};
	return matrix;
}



/**
 * @brief      Afficher une matrice
 *
 * @param[in]  m     Matrice à afficher
 *                   
 * @return     L'erreur
 */
error_t matrixPrint(matrix_t m){
	printf("Matrix : %ld x %ld\n", m.rows, m.cols);
	if(m.matrix!=NULL){
		for(int i=0;i<m.rows;i++){
			for(int j=0;j<m.cols;j++){
				printf("%02d ",m.matrix[i][j]);
			}
			printf("\n");
		}
	}
	return OK; 
}


/**
 * @brief      Libère la place mémoire alloué pour une matrice
 *
 * @param[in]  m     La matrice à libérer
 *
 * @return     L"erreur
 */
error_t matrixFree(matrix_t *m){
	if(m->matrix != NULL){	
		for(int i=0;i<m->rows;i++){
			if(m->matrix[i]!=NULL){
				free(m->matrix[i]);
			}
		}
		free(m->matrix);
		*m=matrixZero();
	}
	return OK;
}



/**
 * @brief      Crée un matrice à partir d'un fichier 
 *
 * @param      fileName  Le nom du fichier
 * @param      matrix    La matrice crée (passage par adresse)
 *
 * @return     L'erreur
 */
error_t matrixCreate(const char* fileName, matrix_t* matrix) {
	error_t error = OK;
	int tmpVal;
	FILE * flot;

    // Open the file
	flot = fopen(fileName, "r");
	if (flot == NULL) {
		error = FILE_NOT_FOUND;
	}
	else{

    	// Lit le nombre de lignes et de colonnes de la matrice
		fscanf(flot, "%ld", &matrix->rows);
		fscanf(flot, "%ld", &matrix->cols);
		if(matrix->rows <= 0 || matrix->cols <= 0){
			*matrix=matrixZero();
		}
		else{
			// Initialise la matrice
			error = matrixInit(matrix);
			if(error == OK){
				// Rempli la matrice avec les valeurs du fichier
				for (int i = 0; i < matrix->rows; i++) {
					for (int j = 0; j < matrix->cols; j++) {
						fscanf(flot, "%d", &tmpVal);
						matrix->matrix[i][j] = tmpVal;
					}
				}
			}
		}
		fclose(flot);
	}
	if(error != OK){
		matrixFree(matrix);
	}

	return error;
}




/**
 * @brief      Retourne si la metrice
 *
 * @param[in]  m     La matrice à tester
 *
 * @return     Si la matrice est vide
 */
bool matrixIsEmpty(matrix_t m){
	return (m.rows == 0 && m.cols == 0) || m.matrix == NULL;
}