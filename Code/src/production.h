/**
 * @brief      Définition du type production
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef PRODUCTION
#define PRODUCTION


#include <stdio.h>

/**
 * Structure représentant une production de produit
 */
typedef struct {
	float cost;
	int factory;
	int period;
} production_t;


/**
 * @brief      Affiche une production sur un flot
 *
 * @param      flot  Le fichier de sortie
 * @param[in]  prod  La production
 */
void productionPrint(FILE * flot, production_t prod);


/**
 * @brief      Affiche une production sur stdout
 *
 * @param      flot  Le fichier de sortie
 * @param[in]  prod  La production
 */
void productionPrintf(production_t prod);



#endif

