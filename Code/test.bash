#!/bin/bash

MATRIX_FOLDER="matrices"

space(){
	for j in `seq $1`; do echo ; done
}

blank_screen(){
	space 1
	read -p "Press enter to continue..."
	#sleep 2
	clear
}


make clean
make
space 3
chmod 000 matrices/fichierQuiNAPasLesDroits.txt
clear


for file in 'matrices/dataNormal.txt' 'matrices/dataNonFinie.txt' 'matrices/dataTropPleine.txt'; do
	for k in 10 -1; do 
		for usine in 0 999 ' '; do 
			echo "Tests de bases avec " $file " :"
			cat $file 2>/dev/null
			tput cup 10 0
			echo './gestion_production' $file $k $usine
			./gestion_production $file $k $usine
			blank_screen
		done	
	done
done


for file in $(ls $MATRIX_FOLDER/*.txt); do
	echo "Test du fichier : " $file " : "
	cat $file 2>/dev/null
	tput cup 10 0 
	echo "./gestion_production" $file "10"
	./gestion_production $file 10
	blank_screen
done

echo "Test d'un fichier inéxistant : "
tput cup 10 0 
echo "./gestion_production fichierQuiNexistePas 10"
./gestion_production fichierQuiNexistePas 10
blank_screen


chmod 755 matrices/fichierQuiNAPasLesDroits.txt




